{ config, pkgs, ... }:

{
	nixpkgs.config.allowUnfree = true;
	
	# System packages
	environment.systemPackages = with pkgs; [
		nil
		helix
		wget
		gnupg
		killall
		btop
	];
	
	# Fonts (TODO: config more details)
	fonts.fonts = with pkgs; [
		cantarell-fonts
		noto-fonts-cjk-sans
		(nerdfonts.override { fonts = ["Hack"]; })
	];

	programs.dconf.enable = true;
	nix.settings.experimental-features = [ "nix-command" "flakes" ];
	system.stateVersion = "23.05";
}
