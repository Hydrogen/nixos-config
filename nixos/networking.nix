{ config, ... }:

{	
	networking.networkmanager.enable = true;
	networking.hostName = "nixos-desktop";
}
