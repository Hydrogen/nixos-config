{ config, pkgs, ...}:

{
	time.timeZone = "Europe/London";

	i18n = {
		inputMethod = {
			enabled = "fcitx5";
			fcitx5.addons = with pkgs; [ fcitx5-mozc ];
		};
		defaultLocale = "ja_JP.UTF-8";
		extraLocaleSettings = {
			LC_ALL = "ja_JP.UTF-8";
			LC_ADDRESS = "ja_JP.UTF-8";
			LC_IDENTIFICATION = "ja_JP.UTF-8";
			LC_MEASUREMENT = "ja_JP.UTF-8";
			LC_MONETARY = "ja_JP.UTF-8";
			LC_NAME = "ja_JP.UTF-8";
			LC_NUMERIC = "ja_JP.UTF-8";
			LC_PAPER = "ja_JP.UTF-8";
			LC_TELEPHONE = "ja_JP.UTF-8";
			LC_TIME = "ja_JP.UTF-8";
		};
	};
}
