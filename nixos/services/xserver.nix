{ config, ... }:

{
	services.xserver = {
		enable = true;
		displayManager.sddm.enable = true;
		desktopManager.plasma5.enable = true;
		videoDrivers = [ "nvidia" ];
	};
	# environment.systemPackages = with pkgs.gnomeExtensions; [
			# pop-shell
			# blur-my-shell
			# places-status-indicator
			# removable-drive-menu
			# user-themes
			# windownavigator
			# appindicator
			# dash-to-dock
			# pkgs.gnome.gnome-terminal
		# ];
		
	hardware.opengl.driSupport32Bit = true;
}
