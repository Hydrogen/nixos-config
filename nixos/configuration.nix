{ config, ... }:

{
	imports =
		[ # Import configuration modules.
			./hardware-configuration.nix
			./boot.nix
			./i18n.nix
			./networking.nix
			./sound.nix
			./users.nix
			./hydronet-ca-trust.nix
			./misc.nix
			# Service configuration files
			./services/openssh.nix
			./services/xserver.nix
			./services/flatpak.nix
			./services/serial-console.nix
			# Programs configuration files
			./programs/gnupg.nix
			./programs/kdeconnect.nix
		];
}
