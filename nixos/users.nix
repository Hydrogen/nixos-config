{ config, pkgs, ...}:

{
	users.users.hydrogen = {
		isNormalUser = true;
		description = "Hydrogen";
		extraGroups = [ "networkmanager" "wheel" ];
		packages = with pkgs; [
			firefox
			nheko
			gajim
			evolution
			keepassxc
			steam
			prismlauncher
			duckstation
			yuzu-early-access
			lollypop
			anki
			git
		];
	};
}
